package com.example.johann.http_get_retrofit_kotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import android.view.ViewGroup.LayoutParams.FILL_PARENT
import android.widget.*
import com.bumptech.glide.Glide


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val url = "http://mobile-courses-server.herokuapp.com/"
        val retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        val service = retrofit.create(CoursesService::class.java)
        val courseRequest = service.listCourses()

        courseRequest.enqueue(object : Callback<List<Courses>> {
            override fun onResponse(call: Call<List<Courses>>, response: Response<List<Courses>>) {
                val t1 = findViewById<TableLayout>(R.id.tableRetrofit)
                val allCourse = response.body()
                if (allCourse != null) {
                    print("HERE is ALL COURSES FROM HEROKU SERVER:")
                    Log.d("TAG", "HERE is ALL COURSES FROM HEROKU SERVER:")
                    for (c in allCourse) {
                        Log.d("TAG", " one course : ${c.title} : ${c.cover} ")
                        val sc = ScoreDb()
                        sc.saveScore("${c.title}",1)
                        Log.d("TAG2", sc.saveScore("${c.title}",1).toString())

                        /* Create a new row to be added. */
                        val tr = TableRow(applicationContext)
                        val ti = findViewById<LinearLayout>(R.id.tableImage)
/* Create a Button to be the row-content. */
                        val t = TextView(applicationContext)
                        t.setText("                ${c.title}")
                    t.setLayoutParams(TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 135))
/* Add Button to row. */
                        val imageView = ImageView(applicationContext)
                        Glide.with(applicationContext).load("${c.cover}").into(imageView)
                        tr.addView(t)
                        ti.addView(imageView)
/* Add row to TableLayout. */
//tr.setBackgroundResource(R.drawable.sf_gradient_03);
                        t1.addView(tr, TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT))

                    }
                    val sc2 = ScoreDb()
                    val reponse = sc2.requestScores()

                    for (i in 0..reponse.size-1 step 2){
                        Log.d("TAG2", reponse[i].pseudo)
                    }
                }
            }
            override fun onFailure(call: Call<List<Courses>>, t: Throwable) {
                error("KO")
            }
        })
    }
}
