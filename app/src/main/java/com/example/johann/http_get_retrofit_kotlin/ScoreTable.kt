package com.example.johann.http_get_retrofit_kotlin

/**
 * Created by johann on 04/12/2017.
 */
object ScoreTable {
    val NAME = "Score"
    val ID = "_id"
    val PSEUDO = "pseudo"
    val SCORE = "score"
}
