package com.example.johann.http_get_retrofit_kotlin

import android.app.Application

/**
 * Created by johann on 04/12/2017.
 */
class MyApp : Application() {

    companion object {
        lateinit var instance: MyApp
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}
