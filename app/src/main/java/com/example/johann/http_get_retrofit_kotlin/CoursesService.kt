package com.example.johann.http_get_retrofit_kotlin

import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by johann on 04/12/2017.
 */
interface CoursesService {
    @GET("/courses")
    fun listCourses(): Call<List<Courses>>
}