package com.example.johann.http_get_retrofit_kotlin

import android.provider.SyncStateContract.Helpers.insert
import org.jetbrains.anko.db.classParser
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*


/**
 * Created by johann on 04/12/2017.
 */

class ScoreDb(private val scoreDbHelper: ScoreDbHelper = ScoreDbHelper.instance) {

    fun requestScores() = scoreDbHelper.use {
        select(ScoreTable.NAME,
                ScoreTable.PSEUDO, ScoreTable.SCORE)
                .parseList(classParser<Score>())
    }
    fun saveScore(pseudo: String, score: Int) = scoreDbHelper.use {
        insert(ScoreTable.NAME,
                ScoreTable.PSEUDO to pseudo,
                ScoreTable.SCORE to score)
    }
    fun saveScores(scoreList: List<Score>) {
        for (s in scoreList)
            saveScore(s.pseudo, s.score)
    }
}
